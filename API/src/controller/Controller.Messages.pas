unit Controller.Messages;

interface

uses
  MVCFramework, MVCFramework.Commons;

type

  [MVCPath('/api')]
  TControllerMessages = class(TMVCController)
  public

  public
    [MVCPath('/messages/receive')]
    [MVCHTTPMethod([httpPOST])]
    procedure Receive(AWebContext: TWebContext);

  end;

const
  ACCOUNT_SID = 'YOUR ACCOUNT SID';
  AUTH_TOKEN  = 'YOUR AUTH TOKEN';

implementation

uses
  System.SysUtils, MVCFramework.Logger, System.Classes, System.StrUtils,
  System.NETEncoding, Service.Messenger,
  Model.Message;

procedure TControllerMessages.Receive(AWebContext: TWebContext);
{$REGION 'Step 1'}
//begin
//  Writeln(AWebContext.Request.Body);
//end;
{$ENDREGION}

{$REGION 'Step 2'}
//var
//  LBody: TStrings;
//begin
//  LBody := TStringList.Create;
//
//  try
//    LBody.Delimiter := '&';
//    LBody.DelimitedText := AWebContext.Request.Body;
//
//    LBody.Text := TNetEncoding.URL.Decode(LBody.Text);
//
//    Writeln(LBody.Text);
//  finally
//    LBody.Free;
//  end;
//end;
{$ENDREGION}

{$REGION 'Step 3'}
//var
//  LMessage: TMessage;
//begin
//  LMessage := TMessenger.Read(AWebContext.Request.Body);
//
//  try
//    Writeln('From: ' + LMessage.From + sLineBreak +
//            'To: ' + LMessage.&To + sLineBreak +
//            'Body:' + LMessage.Body);
//  finally
//    LMessage.Free;
//  end;
//end;
{$ENDREGION}

{$REGION 'Step 4'}
var
  LMessage: TMessage;
  LResponse: String;
begin
  LMessage := TMessenger.Read(AWebContext.Request.Body);

  try
    Writeln('Request: ' + LMessage.Body);

    LResponse := ReverseString(LMessage.Body);

    if LMessage.Body.Trim.ToUpper = 'PING' then
    begin
      LResponse := 'pong';
    end;

    TMessenger.Send(ACCOUNT_SID, AUTH_TOKEN, LMessage.&To, LMessage.From, LResponse);

    Writeln('Response: ' + LResponse);
  finally
    LMessage.Free;
  end;
end;
{$ENDREGION}
end.
