unit Service.Messenger;

interface

uses
  System.SysUtils, System.Classes, System.NETEncoding, Model.Message;

type
  TMessenger = class
  public
    class function Read(ABody: String): TMessage;
    class procedure Send(AUserName, APassword, AFrom, ATo, ABody: String; AMediaURL: String = '');
  end;

implementation

{ TMessenger }

uses Components.Twilio;

class function TMessenger.Read(ABody: String): TMessage;
var
  i        : Integer;
  LMessage : TMessage;
  LBodyText: TStrings;
  LText    : String;
begin
  LText := StringReplace(ABody, '%0A', '%3Cbr%3E', [rfReplaceAll]);

  LBodyText := TStringList.Create;

  try
    LBodyText.Delimiter     := '&';
    LBodyText.DelimitedText := LText;
    LBodyText.Text          := TNetEncoding.URL.Decode(LBodyText.Text);

    LMessage := TMessage.Create;

    for i := 0 to LBodyText.Count - 1 do
    begin
      LMessage.SetProperty(LBodyText.Names[i], LBodyText.Values[LBodyText.Names[i]]);
    end;

    Result := LMessage;
  finally
    LBodyText.Free;
  end;
end;

class procedure TMessenger.Send(AUserName, APassword, AFrom, ATo, ABody, AMediaURL: String);
begin
  TThread.CreateAnonymousThread(
    procedure
    var
      LClient: TTwilioClient;
      LParams: TStrings;
    begin
      LClient := TTwilioClient.Create(AUserName, APassword);

      try
        LParams := TStringList.Create;

        try
          LParams.Clear;

          LParams.Add('From=' + AFrom);
          LParams.Add('To=' + ATo);
          LParams.Add('Body=' + ABody);

          if not AMediaURL.Trim.IsEmpty then
          begin
            LParams.Add('MediaUrl=' + AMediaURL);
          end;

          LClient.Post('Messages', LParams);
        finally
          LParams.Free;
        end;
      finally
        LClient.Free;
      end;
    end).Start;
end;

end.
