program Client;

uses
  Vcl.Forms,
  Client.Main in 'src\Client.Main.pas' {frmMain},
  Components.Twilio in 'components\Components.Twilio.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
