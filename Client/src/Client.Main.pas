unit Client.Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Imaging.jpeg, Vcl.ExtCtrls, Vcl.Imaging.pngimage;

type
  TfrmMain = class(TForm)
    memMessage: TMemo;
    btnSend: TButton;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Image2: TImage;
    Label3: TLabel;
    Label4: TLabel;
    edtMediaUrl: TEdit;
    procedure btnSendClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

const
  ACCOUNT_SID = 'YOUR ACCOUNT SID';
  AUTH_TOKEN  = 'YOUR AUTH TOKEN';


implementation

{$R *.dfm}

uses Components.Twilio;

procedure TfrmMain.btnSendClick(Sender: TObject);
{$REGION 'Exemplo pronto'}
var
  LTwilio: TTwilioClient;
  LParams: TStrings;
begin
  if memMessage.Lines.Text.Trim.IsEmpty then
  begin
    ShowMessage('Digite uma mensagem para ser enviada');
    memMessage.SetFocus;
    Exit;
  end;

  LTwilio := TTwilioClient.Create(ACCOUNT_SID, AUTH_TOKEN);

  try
    LParams := TStringList.Create;

    try
      LParams.Add('From=whatsapp:+14155238886');
      LParams.Add('To=whatsapp:+5511962833166');
      LParams.Add('Body=' + memMessage.Text);

      if edtMediaUrl.Text <> EmptyStr then
      begin
        LParams.Add('MediaUrl=' + edtMediaUrl.Text);
      end;

      if LTwilio.Post('Messages', LParams).Success then
      begin
        ShowMessage('Mensagem enviada!');
      end;
    finally
      LParams.Free;
    end;
  finally
    LTwilio.Free;
  end;
end;
{$ENDREGION}
//
//begin
//  Let's Code
//end;
//
end.
